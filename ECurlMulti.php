<?

require_once 'ECurl.php';

class ECurlMulti {

    /**
     * Список URL.
     * Каждое значение списка может быть как строкой с URL адресом, так и массивом:
     * array(
     *      'url' => 'http://site.com',
     *      'options' => array(...) // Список параметров curl (не обязательно)
     *      'callback' => ... // Функция обратного вызова для этого url (не обязательно)
     * )
     * Параметры curl в этом массиве будут объденены с общими параметрами ({@link options}) и могут переопределять их
     *
     * @var array
     */
    public $urls = array();

    /**
     * Количество потоков для работы. Необходимо только для работы в мультипоточном режиме.
     *
     * @var int
     */
    public $flowCount = 5;

    /**
     * Набор cURL-дескрипторов
     *
     * @var resource[]
     */
    protected $mch;

    /**
     * Функция обратного вызова
     *
     * @var callable
     */
    public $callBack;

    /**
     * Список функций обратного вызова для каждого из перденного url
     *
     * @var array
     */
    protected $callBacks = array();

    /**
     * Список параметров курла
     * По умолчанию имеет одно значение: CURLOPT_RETURNTRANSFER = true
     *
     * @var array
     */
    public $options = array(
        CURLOPT_RETURNTRANSFER => true,
    );

    /**
     * @param array $urls
     */
    public function __construct($urls = array()) {
        $this->urls = $urls;
    }

    /**
     *
     */
    public function exec() {
        $this->flowCount = ($this->flowCount > count($this->urls)) ? count($this->urls) : $this->flowCount;

        $ch         = array();
        $iterations = 0;
        for ($flow = 0; $flow < $this->flowCount; $flow++) {
            $url     = array_pop($this->urls);
            $options = $this->getOptions($url);
            $url     = $this->getUrl($url);

            $curl = new ECurl($url);
            $curl->setOptions($options);
            $ch[$flow] = $curl->curl();

            curl_multi_add_handle($this->multiCurl(), $ch[$flow]);
        }

        $flows = null;
        do {
            do {
                curl_multi_exec($this->multiCurl(), $flows);
            } while ($flows == $this->flowCount); //Ждать пока не отработает хотя бы один поток

            $iterations++;
            $info = curl_multi_info_read($this->multiCurl()); //Получение информации об отработавшем потоке

            $res = curl_multi_getcontent($info['handle']);
            curl_multi_remove_handle($this->multiCurl(), $info['handle']);

            $curlInfo = curl_getinfo($info['handle']);
            call_user_func_array($this->callBack, array(
                                                       $res,
                                                       $curlInfo
                                                  ));

            if (!count($this->urls)) {
                curl_close($info['handle']);
                $this->flowCount--;
            } else {
                //Прописать в отработавший поток новый урл
                $url     = array_pop($this->urls);
                $options = $this->getOptions($url);
                $url     = $this->getUrl($url);

                curl_setopt($info['handle'], CURLOPT_URL, $url);
                curl_setopt_array($info['handle'], $options);
                curl_multi_add_handle($this->multiCurl(), $info['handle']);
                curl_multi_exec($this->multiCurl(), $flows);
            }
        } while ($flows > 0);
    }

    public function close() {
        curl_multi_close($this->multiCurl());
    }

    public function multiCurl() {
        if (!$this->mch) {
            $this->mch = curl_multi_init();
        }

        return $this->mch;
    }

    protected function getUrl($url) {
        if (is_array($url)) {
            return $url['url'];
        } else {
            return $url;
        }
    }

    protected function getOptions($url) {
        if (is_array($url) && isset($url['options'])) {
            return $this->options + $url['options'];
        } else {
            return $this->options;
        }
    }

}