<?
class ECurl {

    /**
     * URL
     *
     * @var array|string
     */
    public $url;

    /**
     * Список параметров курла
     * По умолчанию имеет одно значение: CURLOPT_RETURNTRANSFER = true
     *
     * @var array
     */
    public $options = array(
        CURLOPT_RETURNTRANSFER => true,
    );

    /**
     * Дескриптор cURL
     *
     * @var resource
     */
    protected $curl = null;

    /**
     * @param string $url Url
     */
    public function __construct($url = null) {
        $this->url = $url;
    }

    /**
     * Выполняет curl и возвращает его результат
     *
     * @return mixed
     */
    public function get() {
        return $this->exec();
    }

    /**
     * Выполняет curl в POST запросе и возвращает его результат
     *
     * @return mixed
     */
    public function post() {
        $this->setOptions(CURLOPT_POST, true);

        return $this->exec();
    }

    /**
     * Устанавливает значение параметров Curl`а
     *
     * @param array|int $option Список параметров (параметр => значение) или имя параметра.
     * @param mixed     $value  Значение параметра
     *
     * @return ECurl
     */
    public function setOptions($option, $value = null) {
        if (!is_array($option)) {
            $option = array($option => $value);
        }
        $this->options = $this->options + $option;

        return $this;
    }

    /**
     * Закрывает текущий curl запрос
     */
    public function close() {
        curl_close($this->curl);
    }

    /**
     * Выполняет запрос
     * @return mixed Результат запроса
     */
    protected function exec() {
        $result = curl_exec($this->curl());

        return $result;
    }

    /**
     * Возвращает текущий дескриптор curl. При первом вызове создает его и присваевает ему параметры.
     *
     * @return null|resource
     */
    public function curl() {
        if (!$this->curl) {
            $this->curl = curl_init($this->url);
            curl_setopt_array($this->curl, $this->options);
        }

        return $this->curl;
    }
}